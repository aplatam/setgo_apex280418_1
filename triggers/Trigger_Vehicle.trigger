trigger Trigger_Vehicle on Vehicle__c (before insert, before update) {

	if(Trigger.isBefore && Trigger.isInsert){
		Handler_Vehicle.ActualizaEnProducto(Trigger.new);
	}

	if(Trigger.isBefore && Trigger.isUpdate){
		Handler_Vehicle.ActualizaEnProducto(Trigger.new);
	}

}