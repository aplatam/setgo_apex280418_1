/*#################################### Grupo SETGo #################################
Autor: Ing. Pedro Alan Sierra
Descripción: Clase de prueba del service: Opportunity_Handler_cls
------------------------------------- Log de cambios -------------------------------
Fecha		Versión		Autor							Descripción
----------	-------		----------------------------	-------------------------			
27/01/2018	1.0			Ing. Pedro Alan Sierra			Creación
###################################################################################*/
@isTest
public class Opportunity_Service_tst {

	static{
		//INSERTA USUARIO AQUÍ
		Insert new Opportunity(Name = 'Opp 001');
	}

	public static testMethod void GetParcialidadEnganche(){
		User u = [SELECT Id FROM User WHERE Email = 'TuAdministradorPRD@midominio.com.test'];
		system.runAs(u){
			Opportunity OppCreada = [SELECT Id FROM Opportunity WHERE Name = 'Opp 001'];
			//Inserta aquí tu registro del objeto: Parcialidad__c, ligandolo a la Opp Creada.
			Test.startTest();
			//System.assertEquals(123123, NombredetuClase.getValorParcialidadEnganche(), 'Monto de la parcialidad: ');
			Test.stopTest();
		}
	}

}

/*
//Obtener enganche de la parcialidad
public decimal getValorParcialidadEnganche(){
	decimal ParcialidadEnganche;
	String accId = opp.Id;
	ParcialidadEnganche = [SELECT Monto__c FROM Parcialidad__c WHERE Oportunidad__c = :accId and Tipo__c ='Enganche' ].Monto__c;
	return ParcialidadEnganche;
}

//validacion de fecha de entrega
public Date getFormattedFechaDeEntrega(){
	String EstadoObra = [SELECT Etapa_Obra__c FROM Catalogo_Propiedades__c WHERE Id_Oportunidad__c = :opp.Id ].Etapa_Obra__c;
	if(EstadoObra=='13 PostVenta'){
		Date fechaFirmaCompraEntrega1 = [SELECT fecha_entrega_2__c FROM Parcialidad__c WHERE Oportunidad__c = :opp.Id and Tipo__c ='Saldo'].fecha_entrega_2__c;
		return fechaFirmaCompraEntrega1;
	}
	else{
		Date fechaFirmaCompraEntrega = [SELECT Fecha_de_Entrega_Etapa__c FROM Opportunity WHERE Id = :opp.Id].Fecha_de_Entrega_Etapa__c;
		return fechaFirmaCompraEntrega;
	}
}*/