public class Handler_Vehicle{

	//Este método se debe ejecutar en el Before Insert y en el Before Update
    public static void ActualizaEnProducto(List<vehicle__c> TriggerNew){
        List<Proveedor__c> ProveedoresToUpdate = new List<Proveedor__c>();
        if(TriggerNew.size() > 0 || TriggerNew != null){
	        for(vehicle__c IteraTN : TriggerNew){
	        	if(IteraTN.Proveedor__c != null){
	        		Proveedor__c IndexToAdd = new Proveedor__c();
	        		IndexToAdd.Id = IteraTN.Proveedor__c;
	        		IndexToAdd.LastVehicleRelated__c = IteraTN.Name;
	        		ProveedoresToUpdate.add(IndexToAdd);
	        	}
	        }
	        if(ProveedoresToUpdate.size() > 0){
	        	Update ProveedoresToUpdate;
	        }
	    }
    }

    //No se puede activar el Check Refraction Tire si el numero de piezas es 0
    public static void ValidaNumeroPiezasRefactionTire(List<vehicle__c> TriggerNew){
    	
    }
}