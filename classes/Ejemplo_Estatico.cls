public with sharing class Ejemplo_Estatico {

    static List<String> ListaEstatica {get;set;}
    
    static{
        
        ListaEstatica = new List<String>{'1','a','b'};
        
    }
    
    public static void getInfo(){
        system.debug(':::: ' + ListaEstatica);
    }
    
}