public class Probando_Constantes {
	static final Integer INT_CONSTANTE1 = 1992;
    static final Integer INT_CONSTANTE2;
    
    public static Integer Calculate(){
    	return 2017 - INT_CONSTANTE1;    
    }
    
    static{
    	INT_CONSTANTE2 = Calculate();
        System.debug(':::: ' + INT_CONSTANTE1);
        System.debug(':::: ' + INT_CONSTANTE2);
    }
        
}