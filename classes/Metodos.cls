public with sharing class Metodos {
	
    public String VarPublic{get;set;}
    Private String VarPrivate{get;set;}
    public Static String VarStatic{get;set;}
    
    public Metodos(){
        VarPublic = 'Esto es un texto de la VarPublic';
        VarPrivate = 'Esto es un texto de la VarPrivate';
        VarStatic = 'Esto es un texto de la VarStatic';
    }
    
}