public class ContactSearch {

    public static List<Contact> searchForContacts(string parameter1, string parameter2){
        
        List<Contact> ContactosList = new List<Contact>();
        
        for(Contact QueryContact : [SELECT Id, Name FROM Contact WHERE LastName =: parameter1 AND MailingPostalCode =: parameter2]){
            ContactosList.add(QueryContact);
        }
        
        return ContactosList;
        
    }
    
}